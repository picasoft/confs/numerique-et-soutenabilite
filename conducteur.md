---
title: Conducteur conf SEDD x Picasoft
---

# Conducteur conf SEDD x Picasoft

:::warning
Ce conducteur est en cours de rédaction
:::

:::info
**Plan**
1. Introduction
2. Social
3. Écologique
4. Politique
5. Conclusion

**Durée** : *2 heures (1 heure de conf et 1 heure de discussion libre avec récap à la fin)*

**Description** : Le numérique est aujourd'hui partout. De la médecine aux transports en passant par les paiements, nous l'utilisons tellement qu'il est possible d'oublier l'impact réel d'un tel outil.
Sommes-nous tous égaux dans nos relations au numérique ? Le numérique peut-il être écologique ? Y-a-t-il un lien entre numérique et enjeux politiques ?
Face à toutes ces questions, cette conférence propose de mettre en évidence les rapports entre le numérique et les enjeux sociétaux, environnementaux et politiques.
:::

## Introduction (Roxane)

Picasoft est une association créée en 2016, membre des CHATONS ayant pour but de promouvoir une philosophie libriste et de défendre une approche respectueuse, principalement dans le domaine de l’informatique, que se soit sur le plan de la vie privée, de la liberté d‘expression, de l’environnement ou de la solidarité entre les humains.
Pour ce faire, nous agissons sur trois axes : nous sensibilisons les gens à ces sujets, proposons des formations et hébergeons des services tels que Mattermost ou Etherpad.

Lors de cette conférence, nous allons vous présenter par un tour d’horizon les différents rapports qu’il existe entre le numérique et les enjeux sociétaux, environnementaux et politiques.
Mais pour commencer, il faudrait déjà comprendre ce qu’est le numérique et ce qu’est internet !

### Qu'est ce que le numérique ?

Utiliser le numérique, c’est pouvoir faire une action dématérialisée à partir d’une machine numérique telle qu’un smartphone, une tablette, un ordinateur… Ainsi, tout est virtuel et toute l’action est codée en binaire sous forme d’une suite de 0 et de 1.
Regarder des films, travailler en collaboration, dialoguer, rechercher/consulter des informations, faire des démarches administratives… De plus en plus de choses se font par le numérique et à terme, presque tout le sera. C’est pourquoi les enjeux que nous voulons vous présenter sont importants.

### Qu'est ce qu'Internet ?

Quand nous pensons numérique, nous pensons aussi souvent Internet. Mais qu’est ce donc exactement ? Nous pouvons voir Internet comme une interconnexion massive de toutes les machines du  monde. A l’origine, Internet était un réseau informatique conçu par des militaires dans les années 70 pour s’échanger des mails. Le but de ce réseau était d’être très résistant, même en cas de défaillance : de ce fait, il marche encore 50 ans après sa conception ! Au fil du temps, d’autres machines s’y sont connectées et ses usages ont évolués. Dans les années 90 sont apparues les pages Web. Leur apparition fit réaliser aux gens qu’ils pourraient désormais faire du commerce partout dans le monde depuis chez eux, ce qui fit exploser l’usage d’Internet. Dans les années 2000, grâce à l’augmentation du débit, il y eu une véritable révolution des usages : Internet est devenu multimédia, se transformant ainsi en plateforme d’échange universelle sur laquelle il est possible de trouver de quoi échanger des biens, des services, de quoi se divertir… Cet immense réseau fonctionne grâce à de très nombreux ordinateurs appelés rooters qui maillent la planète et acheminent les données sous forme de petits paquets indépendants. La raison pour laquelle Internet marche correctement est que ces rooters, qui font la liaison entre les serveurs et les utilisateurs d’Internet, tournent 24h/24.

Maintenant que nous en savons plus sur le numérique et Internet, nous pouvons nous demander ce qu’il en est de ses utilisateurs.

### Les utilisateurs

Sur le plan mondial, le taux d’accès à Internet est très variable. Alors qu’en Europe près de 90 % des gens y ont accès, à contrario dans certains pays d’Afrique, ce taux atteint à peine 15 %. En plus d’une fracture au niveau des accès, il existe une fracture au niveau des compétences numériques. En effet, la plupart des gens maîtrise mal Internet et le numérique, ce qui permet à certaines grandes entreprises ou États de profiter de cette méconnaissance pour saper petit à petit le respect de la vie privée et les libertés individuelles.

Nous pouvons donc voir que l’importance qu’ont pris Internet et le numérique ces dernières années a entraîné la création d’enjeux autour de ces thèmes, enjeux que nous allons maintenant vous présenter.

## Social

**Thèmes :**
* Fracture numérique
* Littératie numérique
* Créer du lien *vs.* déshumanisation
* L'informatique doit-elle penser à l'éthique ?
* Les communs
* Décentralisation : créer du lien
* Logiciel libre, une finalité ? un moyen, le seul ?

**Ressources :**
* [Code de conduite du logiciel Funkwhale](https://funkwhale.audio/en_US/code-of-conduct/)
* [Compte rendu, faut-il en finir avec le libre ?](https://dérivation.fr/wp-content/uploads/2021/04/compte-rendu-forum-ouvert-faut-il-en-finir-avec-le-libre-2-4-avril-2021-v1.0.pdf)
* [Podcast LVEL - L'autohébergement, l'alternative aux services en ligne ?](https://radio.picasoft.net/co/2020-10-09.html)
* [Podcast LVEL - Les Communs numériques](https://radio.picasoft.net/co/2020-05-29.html)
* [Podcast LVEL - Culture et numérique : pour le Commun des modèles](https://radio.picasoft.net/co/2020-06-26.html)

Ce serait une erreur de faire du numérique sans se soucier des aspects sociaux, en effet, comme toute technologie le numérique a un impact non négligeable sur la société.

Un premier point auquel on peut penser quand on parle de numérique et de social est le problème de fracture numérique, entre inégalité à l'accès aux technologies numériques, à la formation aux technologies et aux impacts des technologies sur le quotidien de chacun.

:::success
:question: Quelle est la part de la population des Hauts-de-France qui n'a pas d'accès internet à domicile en 2019 ? (2 %, 13 %, 18 %, 27 %)
:::spoiler
> Dans la région, 13 % des habitants n’ont pas accès à internet depuis leur domicile, soit 600 000 personnes. Au niveau national, le manque de compétences et le coût trop élevé du matériel ou de l’abonnement sont les raisons les plus souvent mises en avant par les personnes n’ayant pas accès à internet à leur domicile (respectivement 41 % et 37 %). Seules 5 % déclarent comme motif la non-disponibilité du haut débit à domicile.

Src: INSEE, https://www.insee.fr/fr/statistiques/4986976
:::

:::success
:question: Combien de personnes sont en situation illectronisme en 2019 dans les Hauts-de-France ? (75 000, 420 000, 800 000, 1 500 000)
:::spoiler
> En 2019, sept habitants âgés de 15 ans ou plus sur dix se connectent quotidiennement ou presque à Internet dans les Hauts-de-France. Cependant, 800 000 personnes sont en situation d’illectronisme dans la région (sur 6 000 000 d'hab. environ) : elles n’ont pas utilisé internet au cours de l’année ou n’ont pas les compétences de base. Cela représente un habitant sur six, une proportion comparable au niveau national. L’illectronisme touche en particulier les plus âgés et les pas ou peu diplômés. Le taux d’illectronisme est plus faible dans les grandes agglomérations et le sud de l’Oise et plus élevé dans la Thiérache, le Ternois, le sud du littoral et l’est de la Somme.

Src: INSEE, https://www.insee.fr/fr/statistiques/4986976
:::

:::success
:question: Combien d'entreprises sont comptées comme des entreprises de la BigTech ? (2, 5, 14, 32)
:::spoiler
Les entreprises de la BigTech sont les entreprises qui ont le poids le plus important sur le numérique. On en compte en général 5, les GAFAM, Amazon, Microsoft et Google détiennent 57% de part de marché de l'informatique en nuage, Microsoft et Apple détiennent 95% du marché des systèmes d'exploitation pour ordinateur de bureau, sur mobiles, Google et Apple détiennent à eux seuls quasiment l'intégralité de la part de marché des systèmes d'exploitation, Google et Microsoft se partagent près de 80% du marché des navigateurs, le moteur de recherche de Google domine largement avec 93% de part de marché, ...
Ce ne sont bien sûr pas les seules entreprises ayant un poids très important sur le numérique, il y a les NATU par exemple.
Ce sont toutes des entreprises internationnales aux proportions inimaginables.

À titre de comparaison, dans le collectif des CHATONS il y a 84 structures actives, sans compter le nombre d'autres structures qui adhèrent aux idées ou suivent la charte, ces structures ont toutes des tailles différentes, des formes juridiques différentes, ...
:::

1. Concernant le premier type d'inégalité, on imagine bien que dans le monde l'accès aux technologies est très inégal, les technologies coûtent cher et la répartition des richesses dans le monde est encore très inégale ainsi, assez logiquement, une partie importante de la population mondiale n'a tout simplement pas accès à du matériel ou des infrastructures décentes tandis qu'une autre partie de la population peux se permettre de s'en offrir plus voire trop par rapport à ses besoins réels. Mais même au sein d'une population un peu plus homogène qu'à l'échelle mondiale, en France par exemple, l'accès au matériel et aux infrastructures est encore bien inégal. Ne serait-ce que par rapport aux inégalités territoriales, [par exemple l'accès à la fibre](https://cartefibre.arcep.fr) mais aussi en cette période de pandémie et d'école à distance pour certain, nous avons remarqué que certains foyers n'avaient pas assez d'appareils ou de connexion pour assurer le travail à distance de chacun·es, ce qui pose de réel problèmes. Ce problème est donc un problème réel que ce soit à l'échelle mondiale ou à une échelle plus locale qui découle de problèmes plus généraux d'inégalité des richesses et des territoires.
2. Concernant les inégalités d'accès à la formation au numérique, encore une fois le problème est réel à l'échelle mondiale et locale. Aujourd'hui on voit bien ce qu'est un ordinateur ou internet « en gros » mais il est moins évident de savoir comment ça marche ou comment bien l'utiliser, pourtant ce dernier point n'est pas moins important. Il s'agit du problème de [littératie numérique](https://fr.wikipedia.org/wiki/Litt%C3%A9ratie_num%C3%A9rique), nous en reparlerons un peu plus tard mais le numérique a un impact non seulement sur notre quotidien en tant qu'individu dans une société mais a aussi un impact sur les décisions politiques et est l'objet de décisions politiques, le numérique a imprégné tous les domaines du quotidien en bien comme en mal, il est donc important de bien comprendre ce que c'est, ce qu'on peut faire avec, les dangers, les avantages et les inconvéniants afin d'avoir toutes les informations nécessaires à la prise de décision et pour comprendre le monde qui nous entoure.
3. Enfin, on l'a dit, le numérique a des impacts sur notre quotidien, ces impacts ne sont pas les même pour tous. Il rend la vie plus facile ou plus agréable à certains, sert de source de revenu pour d'autres, remplace les emplois de certains ou encore change nos comportements (surveillance, ...). Fracture entre décideur et utilisateur, l'un a le pouvoir, l'autre est soumis aux décisions qui sont prises, parfois la communication est difficile (Big Tech, qui a déjà discuté avec Sundar Pichai ?)

***Que faire face à tout ça ?***

Vous vous en doutez, en tant que membre d'un CHATONS je vais vous dire que la solution c'est de faire de l'internet décentralisé et du logiciel libre. Mais voyons un peu plus en détail pourquoi cette solution en est une.

:::success
:question: Comment est conçu le réseau Internet ? (distribué, décentralisé, centralisé)
:::spoiler
décentralisé, nous sommes sur un réseau de réseaux. Il n'y a pas un acteur central qui concentre toutes les connexions mais des acteurs plus locaux qui gèrent quelques connexions et ces acteurs se connectent entre eux.
:::

Déjà parlons du web, l'aspect le plus important selon moi est de décentraliser, en effet en décentralisant on supprime le pouvoir d'une grosse entité comme le sont par exemple les GAFAM. Leur pouvoir ne vient que du fait que tout passe par eux, leurs choix impact une part très significative des usagers du web. C'est tout a fait possible grâce à la conception d'Internet, en effet c'est un réseau décentralisé avec l'intelligence en périphérie du réseau, le reste peut-être vu comme un bête réseau de tuyaux avec des intersections plus ou moins grosses, (parler de la notion de "*stupid pipe*"). Cela veut dire que n'importe qui voulant mettre un ordinateur avec son site web dessus le peut, il n'y a pas besoin de demander l'autorisation à Google. C'est ce qu'on fait chez Picasoft avec nos services web et on encourage chacun·es le souhaitant à le faire. En fait ce n'est vraiment pas en accord avec l'architecture d'Internet d'avoir des gros acteurs centraux comme les GAFAM sur le web, en plus de leur donner un pouvoir démesuré cela augmente le risque de panne catastrophe (cf. pannes AWS, Google, ...) et quand un service n'est plus rentable pour eux ils peuvent décider de le supprimer du jour au lendemain, nous obligeant ainsi à modifier nos habitudes. En plus on peut parler beaucoup plus facilement à un particulier, une association ou une petite entreprise qu'à une multinationale.

Ensuite le LL, il est très important car en plus de nous donner confiance dans les technologies que nous utilisons, il permet de voir comment elles sont conçues, les rendre intéropérables pour changer de logiciel si le notre ne nous conviens plus, ... Le fait de pouvoir étudier les sources permet de voir au moins dans les grandes lignes le fonctionnement, d'un système d'exploitation, d'un protocole réseau, ... Si nous n'avons pas les compétences pour aller lire le code directement, nous pouvons en général faire confiance à une large communauté qui a pu elle lire le code sans être lié par des intérêt financiers ou autres, cette communauté sera à même de vulgariser la technologie afin de la rendre accessible en se basant sur son expérience, ce qu'elle a vu et étudié et pas en ressortant les seules documentations fournies par l'éditeur de la technologie. Cela participe au principe de littératie numérique et au partage de la connaissance, mais ne suffit pas à lui seul, il faut aussi faire de l'éducation populaire, ...

Bien sûr le LL ne suffit pas, déjà les logiciels ne sont pas les seuls objets qui peuvent être libérés, il existe d'autres objets comme les livres, les musiques, les contenus vidéos, ... On préfèrera parler de culture libre pour être plus général. Par exemple il est bien d'avoir un logiciel qui est libre mais pour le comprendre il faut être un minimum formé sur le sujet, et afin que le plus de monde possible puisse être formé sur le sujet il faut pouvoir accéder librement à des ressources, des cours, des manuels, ... Et pour améliorer ces contenus, les mettre à jour les corriger, ... il faut les libérer au sens de la culture libre.

:::success
:question: Qu'est-ce que le cloud ? (Un site web quelque part dans le ciel (probablement dans un satellite), Le PC de quelqu'un d'autre, Une application qui tourne sur mon PC)
:::spoiler
Le PC de quelqu'un d'autre, plus de détails après.
:::

Aussi, aujourd'hui nous n'utilisons pas que des logiciels que nous installons sur nos ordinateurs mais des services en ligne dans le « cloud » donc sur le PC de quelqu'un d'autre, son logiciel a beau être libre, il a accès à nos données et contrôle le service que nous utilisons, il faut pouvoir lui faire confiance, discuter avec lui, savoir comment il gère tout ça. Une initiative comme celle des CHATONS est très utile dans ce sens (expliquer l'acronyme).

Ce que nous cherchons à faire finalement n'est pas juste un logiciel avec une licence libre mais à créer des communs et les communautés qui vont avec, le logiciel libre n'est donc pas un but final mais un moyen d'y arriver. Ainsi on crée du lien entre les personnes qui contribuent et utilisent un logiciel, un service, une oeuvre. On cherche à se faire rencontrer des personnes qui partagent des valeurs et des intérêts communs. Une licence libre ne suffit peut-être pas à créer ce genre de communauté, le projet Funkwhale par exemple en plus d'une licence libre a adopté [un code de conduite](https://funkwhale.audio/fr_FR/code-of-conduct/) dans lequel chaque membre de la communauté s'engage à faire un usage le plus inclusif possbible du projet (sans harcèlement, sexisme, homophobie, ...). Certains diront que ce code de conduite va à l'encontre des principes du logiciel libre puisque nous n'avons pas une liberté d'utilisation pour quelque usage que ce soit, mais le but est d'avoir une communauté unies ou chacun·es est à sa place puisque tolérer certains comportements déplacés pourrait revenir à exclure certaines personnes de la communauté qui ne se sentiraient pas à l'aise, on est en fait face à un problème de définition de la liberté (ex: liberté d'expression à l'européenne *vs.* à l'étatsunienne).

## Écologique

**Thèmes :**
* Matérialité numérique
    * Matières et énergies
    * Poids du cloud et effets rebonds
* Etude d'impacts par cycle de vie
    * Extractions / Fabrication
    * Usage
    * Fin de vie et recyclage
* Sobriété numérique
    * Eco-gestes

### Intro (Pierre)

* Les préoccupations environnementales du numérique sont récentes
* aujourd'hui pas de consensus de méthode ou d'objectivité dans les résultats.
* La posture principale des recherches liant écologie et numérique est celle de la prudence, les chiffres utilisés doivent être pris avec recul, en tant que premières indications, car basés sur des modèles partiels.

*Exemples de mesures : emissions de GES d'activités numériques (limité car dépend du modèle utilisé), poids d'une page web / complexité de l'architecture, consommation électrique du matériel

:::success
:question: Quelle est la part du numérique dans les émissions mondiales de $CO_2$ ? (1%, 2% [autant que l'aviation civile], 4%, 6%)
:::spoiler
4% en 2021 d'après ADEME et TheShiftProjet
:::

//

Green IT / Green By IT : posture visant à reconnaitre les pollutions numériques tout en insistant sur le fait que les TIC rendraient possible des usages « verts », dans les autres secteurs de l'économie, en se substituant à des usages « sales » (moins de consommation de papier, optimisation de la production) : "Ok c'est 4% mais le problème ce sont les 96% autres !!"

Au-delà d'un discours marketing qui vanterait l'idée que le numérique pourrait verdir une diversité de domaines d'activités par le biais des économies que sa rationalisation et son contrôle offriraient, de nombreux travaux alertent sur le fait qu'aujourd'hui, le secteur numérique "n'offriraient aucunes garanties sur la question environnementale".

Pour G. Roussihle, chercheur spécialisé dans le design et la sobriété numérique, la littérature "solutionniste" existante se baserait sur des scénarios trop optimistes, et prendrait pour acquis le fait que le numérique substituerait aux usages qu'il remplacerait. Or sur ce dernier point, la tendance est à l'inverse. On observe des logiques d'empilements (le cout écologique du numérique s'ajoute aux autres et ses progrès ne compensent pas son impact) et la majorité des gains et optimisations engendrées par le numérique sont davantage employés à des fins de productivité et de production, plutôt qu'à des objectifs de stabilisation, voire de diminution des consommations d'énergies et de matières.

Certes, la part d'énergie renouvelable augmente, en particulier dans secteur numérique où de grands acteurs vantent leurs efforts et objectifs de neutralité carbone pour un avenir proche
mais cet apport est surtout employé à décarboner leur croissance en monopolisant la production d'énergies renouvelables plutôt qu'à participer à une transition globale vers la sobriété.

### Matérialité (Pierre)

:::success
:question: Combien pensez-vous qu'il y ait de **milliards** de smartphones actifs dans le monde en 2020 ? (6 Mds, 10 Mds, 12 Mds, 14 Mds [mars 2020 : 7,8 milliards d'humains])
:::spoiler
14 Mds de Smartphones actifs d'après Réparer le futur de Inès Leonarduzzi
:::

//

4% des émissions mondiales de $CO_2$.
Dans ces emissions, 47 % seraient liées aux équipements des utilisateurs (ordinateurs, smartphones, objets connectés, boxs internets...), ce qui peut sembler normal.


//

On se doute tou.te.s que pour fabriquer ces équipements, il est nécéssaire d'utiliser des ressources comme des métaux, du verre voire du plastique et de l'énergie.


![Schema ADEME composants d'un smartphone](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.x_snkkgesBNxSCNsWOOYUQHaIX%26pid%3DApi&f=1 =200x)



 En revanche, se dire que le reste, soit 53% des émissions carbones du numérique, serait lié aux centres de données (*Data Centers*) et aux infrastructures réseaux, peut surprendre.


//

#### Infrastructure physique du numérique

Internet : infrastructure d'interconnexion de réseaux
Web : service permettant l'accès à des informations sur Internet

Derrière le *cloud*, le *web* ou encore Internet se cachent en réalité des quantités astronomiques d'équipements relais, de cables sous-marins intercontinentaux (*backbone*) et d'entrepôts d'ordinateurs répartis sur toute la surface du globe. Ces usines à numérique, les Data Centers, fonctionnent 24/24h, 7/7j pour fournir à toute heure un service fonctionnel et instantané, peut importe l'endroit ou l'on se trouve.

//

##### Les ressources du numérique :
* une cinquantaine de métaux ;
    *  terres-rares : quantités moindres au tonage (pour 1 tonne de terre) que les métaux industriels
    *  métaux rares particuliers dotés de propriétés réactives, électromagnétiques et lumineuses
* de l'énergie pour l'extraction, le transport, la transformation ;
* des matières premières (caoutchou, eau...)

Transport des ressources par bateau / camion et transformation en matériaux pour la création de composants électroniques (supra-conducteurs)
*Ex : purification et fonte du cuivre, de l’or, du platine, des métaux rares, etc*

//

##### Les Centres de données

![Google Data Center, Oregon](https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Google_Data_Center%2C_The_Dalles.jpg/1920px-Google_Data_Center%2C_The_Dalles.jpg)

* hébergement de rangées de serveurs stockant, calculant ou transferrant des données, système de climatisation et équipement réseaux pour les télécoms et l'interconnexion à d'autres centres et réseaux.

* prend en entrées des données, du Froid (eau, air) et de l'electricité et renvoie des données et de la chaleur en sorties.

*Certains des plus gros data centers du monde utilisent jusqu'à 1 600 000 L d'eaux par jour pour leur refroidissement d'après un rapport de l'ADEME dédié à l'impact énergétique et spatial des Data Centers*

Une fois installé, un centre de données réserve une partie de la puissance électrique d’un territoire, parfois une partie d’un flux d’eau en même temps qu’il rejette plus ou moins de chaleur et influence le marché foncier. => Y compris des impacts sociales ! *Pas mal pour un truc immatériel*

//

##### Les cables sous-marin

Permettent de faire qu'une donnée située sur un DC de Google puisse arriver chez vous à la moindre requête !!

Entre les US et la FRA, compter environs 7000 km de cables

En 2020 : 406 câbles sous-marins dans le monde, longueur cumulée de 1,2 millions de kilomètres.

Un cable de 10 cm de diamètre composé de fibres optiques enroulées dans du caoutchou ou du kevlar, déposé par bateau au fond des océans.

Durée de vie minimum : 25 ans mais régulièrement remplacé plus tôt pour des cables plus performants en terme de débits.

*Conséquence :  En 2017, le câble reliant Madagascar à l’Afrique du Sud a été sectionné suite un séisme, provoquant une coupure au réseau Internet pendant 12 jours.*

Coup du déploiement d'un cable transatlantique ± un milliard de dollars. => Ce sont généralement des consoritums qui co-investissent, mais depuis qq temps, ce sont aussi les GAFAM pour prioriser les routes dont ils ont besoins
*Jupiter, cable sous-marin de 14 000 km de long reliant l'Asie (Japon & Philippines) à l'Amérique du Nord (Los Angeles) annoncé en 2017 par Amazon, Facebook et Soft Bank pour faire face à l'augmentation du traffic de données dans le Pacifique.*

![Carte mondiale des cables Internet 2019](https://veillecarto2-0.fr/wp-content/uploads/2019/01/maillage_mondial_c%C3%A2ble_telegeography.png =350x)


//

##### Les équipements utilisateur

* Tout appareil connecté implique le transfert de données sollicitant des centres de données et le réseau, en utilisation comme en veille.
    *    objets personnels, équiments de sécurité, capteurs...

* Caractérisés par leur volume
    * 2019 : 37 Mds d'équipement informatique
    * 3,5 milliards de smartphones, :warning:
    * 3,8 milliards d’autres téléphones
    * 3,1 milliards de télévisions/ écrans/vidéoprojecteurs
    * à peu près 19 milliards d’objets connectés

//

##### Quelques mots sur la consommation énergétique

Après fabrication, livraison, vente et installation, la consommation énergétique des appareils numériques : l'electrique.
Des efforts importants sont fait sur l'amélioration de l'efficaté énergétique.

*Ex : En 2020, les centres de données consomment toujours la même quantité d’électricité qu’en 2015, voire moins, alors que leur charge de travail a été multipliée par 2,7 et que le trafic a été multiplié par 4*

De manière globale, tendance à l'efficacité energétique, pourtant la consommation électrique du numérique ne diminue pas, non seulement car celle-ci ne s'applique que pour l'utilisation des équipements, et du plus, des equipements plus efficaces sont souvent plus utilisés

*Ex : Dans l’exemple donné plus tôt les centres de données consomment le même niveau d’électricité alors que le trafic a été multiplié par
4 entre 2015 et 2020. Si le trafic était le même alors nous aurions pu diviser la consommation électrique des centres de données par 4*

=> Paradoxe de Jevon / Effet Rebond : lorsque qu’un équipement devient plus efficace, il devient alors moins cher à utiliser, entraînant une hausse de consommation.

  * D'après Clicking Green, rapport de Green Peace, le fonctionnement d'Internet représente 7 % de la consommation électrique mondiale

Cependant, l'efficacité énergétique et la consommation électrique ne peuvent être des indicateurs fiables de la consommation énergétique réelle. Par exemple, elles ne donnent pas d'infos sur l'énergie nécéssaire à la fabrication des équipements, ni sur le type d'énergie utilisée pour produire de l'electricité [Dans le monde, très grande majorité de l'electricité consommée est produite à base d'énergies fossiles ayant un impact sur le dérèglement climatique.].

//

Le numérique est un système matériel qui sollicite  de nombreux écosystèmes pour extraire les matières premières nécéssaires à la production de ses composants, s'appuie sur les chaines d'approvisionnement/production globales pour livrer des produits numériques partout.  nécéssite de l'énergie à tous les niveaux et produit des GES. Cette infrastructure produit également des déchets, souvent toxiques, très peu recyclé ou recyclables.

Tout son fonctionnement repose sur des architectures physique nécessitant des quantités importantes d'énergies à tous les niveaux.

Plutôt que de chercher à savoir quel monde est possible grâce au numérique, cherchons plutôt à comprendre quel numérique(s) est possible dans le monde et ses limites.

//

### Cycle de vie materiel (Emma)

:::danger
IN PROGRESS je bosse en local, dès que c'est ok je le repasse ici !
:::

L'ACV (Analyse Cycle de Vie) d’un produit ou d’un service consiste à inventorier les flux de matières et d'énergies entrants et sortants à chaque étape du cycle de vie d'un produit. A partir de ces données, on procède à une évaluation des impacts environnementaux.

Pourquoi c'est particulièrement compliqué avec le numérique ? (les limites de Roussihle)
* Pas de méthodologie commune => pas d'objectivité des resultats (Estimations et ordres de grandeurs)
* Données privées souvent confidentielles : pas de falsifiabilité des resultats
* Fortes imbrications des causes et des effets (pas possible de déterminer précisément responsabilité utilisateurs, réseau, DC)
* Nature transverse du numérique : secteur en soi, mais également dispersé dans d'autres secteurs (l'impact d'un scanner IRM connecté, numérique ou médical ?)

#### matières premieres

* Extraction de terres rares / raffinage : usage de polluant (acides / métaux lourds) purification de terres rares : 200 m3 d'eau alors souillées évacuée majoritairement sans traitement vers les fleuves / nappes phréatiques
* Parler un peu des terres rares


#### fabrication
* anatomie d’un smartphone ou d’un ordinateur révèle une quarantaine de métaux différents dans ses composants.
* 50 à 350 fois leur poids en matières pour produire desappareils électriques à forte composante électronique,soit par exemple 600 kg pour un ordinateur portable et 500 kg pour une box Internet.

On passe sous silence transport et distribution
#### utilisation
#### valorisation et fin de vie
* Très peu de recyclage
    * Seulement 15 % des téléphones sont collectés pour être recyclés/dépollués en France.
    * Phénomène de mines urbaines (plus de 50 millions de smartphones "dorment" en France)
    * Cout exorbitant
* Plupart des métaux contenus recyclables / réutilisables d'autres sont dangereux et doivent subir un traitement spécifique
* Déportation des déchets électroniques / invisibilisation dans les gros pays consommateurs
    * Villages du cancer en Chine, villes décharges

fin de vie sans valorisation :
- perte de matériaux
- pollution des sols croissante et largement ignorée

Passage interessant de "Situer le Numérique" :
"La vie d’un équipement utilisateur ne s’arrête pas à la production et à l’usage, il y a aussi la fin de vie. Que se passe t-il lorsqu’on amène sa télévision à la décharge ou lorsqu’on jette son smartphone ? En 2016, On estime que 44,7 millions de tonnes métriques de déchets d’équipements électriques et électroniques (DEEE) ont été générées. Cela représente en une année une masse équivalente à 4 500 tours Eiffel ou 6,1 kilos par habitant . Cette masse d’équipements en fin de vie continue à augmenter à cause du renouvellement trop rapide des équipements. Dans le cas du smartphone, on a estimé sa durée de vie à 21,6 mois en 2015, soit moins de 2 ans . Cela est lié aux campagnes marketing qui encourage ce renouvellement, mais aussi lié aux différents types d’obsolescence (logicielle, matérielle, psychologique) et de la masse de livraisons annuelles à écouler. Les métaux et les composants de ces équipements sont peu, voire très peu recyclés sauf pour certains cas comme le cuivre sans que ce recyclage implique la fin de l’extraction minière du cuivre. Beaucoup de cuivre est immobilisé dans le bâti et les infrastructures et ne repart dans les circuits de production.""

### Sobriété Numérique (Lucas)
:::success
:question: Quelle est la durée d'usage moyenne d'un Smartphone en France ?
:::spoiler
2 ans d'après l'ADEME, et dans plus de 80% des cas, le Smartphone remplacé est toujours fonctionnel
:::

:::success
:question: Combien de requêtes Google sont effectuées chaque seconde ?
:::spoiler
80 000 requêtes
:::

*(expression forgée en 2008 par GreenIT.fr)*
- le numérique ne peut pas être écologique par nature (un site web consomme de l'électricité, et seulement 12% de l'électricité consommée en France provient d'énergies renouvelables - *Ministère de l'intérieur*), il faut chercher à le rendre plus sobre (distinguer sobriété des usages de sobrité de la sobriété des services proposés )
- Multiplication des points l'accès : pour acceder à des services numérique une meme personne peut maintenant posseder une télé, un ordi, une tablette, un smartphone...

C'est donc, dans un premier temps, à une échelle plus petite que nous pouvons avoir un impact positif. D'un côté, il existe différents moyens pour les créateurs de sites web pour limiter l'émission carbone, cela peut passer par la compression des images et la réduction du contenu au strict nécessaire (cf webdesignfortheplanet.com). Ensuite, il existe, pour le "consommateur" de contenu sur le web, différents gestes qu'il peut adopter et que nous allons voir à présent.

#### Eco-gestes Numériques
Dans la mesure ou l'ensemble de CdV est extrèmement polluant, en particulier pour la fabrication et la fin de vie, l'une des premières bonnes pratiques et de garder ses appareils le plus longtemps possible.
* Réduire sa consommation
    * Se désabonner des newsletters inutiles (conseiller l'utilisation de Cleanfox ?)
    * Desinstaller les applications addictives
    * Supprimer les applications fantomes
        * Pour [Android Authority](https://www.androidauthority.com/77-percent-users-dont-use-an-app-after-three-days-678107/), 77% des utilisateurs n'utilisent plus une app 3 jours après son intallation (le chiffre monte à 90% après 30 jours). Or ces apps consomment de l'énergie et continue d'être mises à jour en arrière-plan.
* Mieux consommer
    * Mode sombre pour les écrans OLED/AMOLED eteint les pixels affichant du noir : économies d'énergie
    * Privilégier l'éthernet au WIFI et le WIFI aux données celullaires (D'après Inès Leonarduzzi, le wifi consomme 23 fois moins d'énergie que le réseau 4G, utilisant des antennes relais)
    * Eteindre ses appareils plutot que les mettre en veille
    * Utiliser des bloqueurs de publicités
    * Réparer son matériel et si un changement est inévitable, privilégier l'achat reconditionné
    * Télécharger des médias plutot que les lire en streaming
    * Ne pas "laisser dormir" son matériel dans un tiroir
        * Une des causes du très faible recyclage :
    * Eviter l'installation systématique des mises à jour fonctionnelles / cosmétiques qui ralentissent les machines [obésiciels]. Appliquer les màj de sécurité.

#### Les positions des institutions publiques en France

##### Au niveau du parlement

* LOI n° 2020-105 du 10 février 2020 relative à la lutte contre le gaspillage et à l'économie circulaire
    * Les producteurs, importateurs, distributeurs ou autres metteurs sur le marché d'équipements électriques et électroniques communiquent sans frais aux vendeurs de leurs produits ainsi qu'à toute personne qui en fait la demande l'indice de réparabilité de ces équipements ainsi que les paramètres ayant permis de l'établir. Cet indice vise à informer le consommateur sur la capacité à réparer le produit concerné. (article 16)
    * Au premier janvier 2022, les FAI sont obligés d'indiquer la quantité de data consommée ainsi que l'équivalent carbone (article 13.3)
    * Article 58: A compter du 1er janvier 2021, les biens acquis annuellement par les services de l'Etat ainsi que par les collectivités territoriales et leurs groupements sont issus du réemploi ou de la réutilisation ou intègrent des matières recyclées dans des proportions de 20 % à 100 % selon le type de produit.
* Le Sénat a établi 25 propositions pour un numérique plus sobre
* L'ADEME et l'ARCEP ont lancé un projet pour mesurer l'empreinte environnementale du numérique en France (pour aider à l'établissement de politiques publiques)
Beaucoup de ces mesures sont le fruit d'années de travail d'organisations françaises (GreenIT, the Ecoinfo research group, the Shift Project, etc)

**Ressources :**
* [20 statistiques sur l’impact environnemental de nos activités numériques](https://www.webdesignfortheplanet.com/20-statistiques-impact-environnemental-numerique/)
* [La sobriété numérique en perspective](https://la-rem.eu/2021/01/la-sobriete-numerique-en-perspective/)
* [L'obsolescence dans le numérique](https://www.halteobsolescence.org/lobsolescence-dans-le-numerique/)
* [Metaux et numérique](http://conseil.gauthierroussilhe.com/post/metaux-numerique.html)
* [Eco-gestes numériques](https://www.webdesignfortheplanet.com/reduire-impact-environnemental-internet/)
* [Situer le numérique](https://designcommun.fr/cahiers/situer-le-numerique/situer-le-numerique.pdf)
* [Que peut le numérique pour la transition écologique ?](http://conseil.gauthierroussilhe.com/pdf/NTE-Mars2021.pdf)
* [Consommation et production responsable](https://www.un.org/sustainabledevelopment/fr/wp-content/uploads/sites/4/2020/03/ODD_12_Rapport2019.pdf)
* [Soutenabilité numérique: état des lieux en France](https://gauthierroussilhe.com/fr/posts/digital-sustainability-a-french-update)

## Politique

**Thèmes :**
* Modèle économique/financement (anticapitalisme/...)
* Gouvernance des projets libres (Wikipedia/Linux/Debian/Funkwhale/Picasoft :grin: )
* Capitalisme de surveillance
* Copyright *vs.* "Copier c'est aimer"
* Les communs comme vision politique, tragédie des communs
* Problèmes de la dépolitisation des technologies numériques ?
* Pouvoir de la technique, des logiciels ?

**Ressources :**
* [Compte rendu, faut-il en finir avec le libre ?](https://dérivation.fr/wp-content/uploads/2021/04/compte-rendu-forum-ouvert-faut-il-en-finir-avec-le-libre-2-4-avril-2021-v1.0.pdf)
* [Podcast LVEL - Les financements du libre, épisode 2 : l'économie de fonctionnalité](https://radio.picasoft.net/co/2021-01-08.html)
* [Podcast LVEL - Les financements du libre - Épisode 1 : Les fonctionnalités payantes](https://radio.picasoft.net/co/2020-10-30.html)
* [Podcast LVEL - Le capitalisme de surveillance](https://radio.picasoft.net/co/2020-04-24.html)
* [Poscast LVEL - Les communs numériques](https://radio.picasoft.net/co/2020-05-29.html)

La politique n'est pas à séparer du numérique, on l'a vu en première partie, les deux sont intrinsèquement liés. Et la politique ce n'est autre chose que l'organisation d'une communauté, le mode de prise de décision, ... D'une part les projets libres représentent pour certains des masses économiques et stratégiques considérables et d'autre part chez les CHATONS et dans les milieux libristes ont est très forts pour trouver des moyens de gouvernance plus ou moins originaux.

Déjà niveau organisation de la communauté, il existe deux grandes catégories d'organisations, les modèles verticaux assez classiques (dictateur bienveillant à la Linux, association avec un organe de décision/bureau avec un fort pouvoir à la FSF, entreprise avec un directeur) et des modèles horizontaux (Debian avec une constitution et d'autres documents fondateurs, Picasoft et la toux-doux-cratie, [funkwhale](https://funkwhale.audio/fr_FR/collective) avec 3 organes centraux et une forte place à la démocratie), nous pouvons ainsi acquérir une certaine expérience sur ce qui fonctionne bien ou mal dans chaque modèle, dans quel contexte et pourquoi.

Ces modèles d'organisation sont très importants car ils définissent en partie l'orientation du projet, faire un truc qui convient bien à une personne et les autres peuvent contribuer à ce truc mais sans en décider l'orientation ou une communauté réfléchit ensemble a un truc qui convient bien à la communauté. C'est très important à tel point que qu'il y a des mainteneurs de projets qui (parfois des entreprises) qui n'écoutent pas asseze bien la communauté au point qu'il s'opère parfois ce qu'on apelle un *fork*, la communauté n'est plus du tout en phase avec le mainteneur et décide de créer son propre projet en repartant sur les mêmes bases mais avec une orientation et parfois une organisation différente.

:::success
:question: Parmi ces projets, lequel n'a pas connu un fork majeur dû à une divergence de point de vue entre la communauté et le mainteneur ? (Open Office, CodiMD, MySQL, Mastodon)
:::spoiler
Tous ces projets ont connu des forks, en effet les forks sont très courants et sont même une force du logiciel libre, on est pas obligé d'être d'accord avec les mainteneurs d'un logiciel pour l'utiliser, et si celui-ci prend une orientation qui ne nous plaît pas, on fork et on crée un nouveau projet, une nouvelle communauté, ...
:::

En réalité, dès que le LL s'est heurté à des sujets politiques, la communauté s'est divisée, par exemple, avec gab, un réseau social de l'extrême droite américaine qui est arrivée sur Mastodon, la communauté n'en a pas voulu mais il existe plein d'autres réseaux fascites qui utilisent les logiciels que nous utilisons. Il vaudrait peut-être mieux leur interdire l'utilisation de nos technologies ?

:::success
:question: Sur une échelle de 1 à 5 quelle appréciation avez-vous du mot "politisé" (1 : -- ; 5 : ++) ?
:::spoiler
Rappeler la définition de la politique : l'organisation de la vie en société
:::

Un gros problème que l'on peut rencontrer avec des projets informatiques, c'est qu'ils pensent qu'il n'est pas de leur devoir de se soucier des enjeux ethiques, ils veulent paraître totalement neutre et dépolitisés, est-ce vraiment rendre service au bien commun que de ne pas se soucier de ces enjeux ? Par exemple, un missile lancé grâce à des logiciels libres est plus ethique ? Il peut être nécesaire de poser de réfléchir à l'usage du projet au moment de la conception, par exemple quand on fait un service web, on peut être ni contre ni pour la collecte de données mais quand on peut ne pas passer par des appels aux serveurs de Google c'est rendre service à tout le monde de ne pas le faire.

On peut se poser la question de pourquoi la technique est dépolitisée ? Pourquoi les gens qui développent sont en général peu politisés ? En fait les personnes qui font de la technique en général aime faire les technologies avant de penser à l'idée politique qu'elle implique derrière. Dans les UT on a la chance de pouvoir avoir un reflexion sur cette question même si on voit bien que beaucoup ne la poussent pas bien loin, et pire de manière générale (hors UT) beaucoup de personnes ne se posent même pas la question. En fait le problème est plus général, c'est celui d'une dépolitisation générale de la société qui va même jusqu'à donner un sens négatif au mot *politisé* (_C'est une itw de 2015 que j(Rémi)'ai vu parce que je bosse dessus, si je suis là pour le dire ça peut être pertinent, mais si c'est dit par quelq'un d'autre c'est chelou de ressortir un itw du VP du Medef d'il y a 6 ans._ exemple de l'interview du VP du Medef qui quand l'interviewer Axel de Tarlé lui demande si le problème c'est que les syndicats de travailleurs sont politisés et qu'il répond que oui c'est un gros problème, or c'est la raison d'être des syndicats). Donc peut-on changer ça avec et dans le numérique ? La question se pose.

Faire, contribuer ou utiliser des projets libre c'est créer des communs. Une des critiques qui revient souvent quand on parle de communs est la tragédie des communs, c'est l'idée que quand il existe une ressource mise en commun pour une communauté, elle finit tragiquement par être surexploité et donc par devenir inutilisable à cause de l'égoïsme de chacun·es. La solution serait la privatisation ou la nationalisation selon Hardin. Mais dans la réalité une communauté peut bien gérer voire mieux une ressource (étude de Elinor Ostrom), coopération et pas d'individualisme avec pour seul but le profit maximal, il faut notamment des règles, la capacité de faire et modifier les règles, la capaciter à faire respecter ces règles et un mécanisme de résolution de conflit peu couteux.

Autant que pour les modèles d'organisation, il y a une grande diversité des modèles de financement dans les projets libres, entre les projets qui se financent par du contenu exclusif ou sur mesure, d'autres en proposant des services d'hébergement, d'autres par du support professionnel ou encore d'autres exclusivement par la générosité des utilisateurs ou par des mécènes, certains projets s'intègrent plus ou moins bien dans une économie capitaliste et abandonnent ainsi certaines valeurs militantes pour faire plaisir aux investisseurs (des logiciels qui font de l'*open source* ie. du libre sans l'éthique, ils utilisent des licences libres purement pour l'efficacité du modèle) quand d'autres revendiquent un modèle anticapitaliste (libérer les utilisateurs, l'humain en passant par les licences libres, on peut d'ailleurs se demander si les licences libres sont suffisantes, ex de Funkwhale).

## Conclusion

A quel point faut-il des chiffres pour se bouger ? Doit-on vraiment avoir des chiffres ? on en sait rien mais c'est déjà trop !
**+**
Tout n'est pas perdu, il existe des pistes, LL, projets d'info éthique, respectueuse de la vie privée/environnement
**OU/ET**
Une conclusion interactive ? Demander à l'assembler de résumer les concepts et avoir un "plan de secours" ? À voir pour les outils (posts-it, pads, sous-salons)

> Le fait de faire du logiciel libre, c’est un outil au sein d’une communauté . L’objectif c’est pas de faire du libre pour du libre c’est faire du libre pour autre chose. Le libre comme outil et pas comme fin en soit. Donc utilisons le libre pour créer un numérique sobre, sain et inclusif.

Passer un pad de ressources à la fin ? Mener vers nos émissions, nos conférences, nos ressources pour cette conf, ...
