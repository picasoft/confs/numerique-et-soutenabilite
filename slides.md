---
type: slide
slideOptions:
  transition: slide
  theme: solarized
title: Conférence sur le numérique durable
---

# Numérique durable

## Picasoft × SEDD

---

# Introduction

* Qui sommes nous ?
* Qu'est ce que le numérique ?
* Qu'est ce qu'Internet ?
* Les utilisateurs

---

# Social

1. Fracture numérique
2. Décentralisation
3. Licences libres

----

## Quizz

:one: Quelle est la part de la population des Hauts-de-France qui n’a pas d’accès internet à domicile en 2019 ?

:two: Combien de personnes sont en situation illectronisme en 2019 dans les Hauts-de-France ?
<!-- .element: class="fragment" data-fragment-index="1" -->
:three: Combien d’entreprises sont comptées comme des entreprises de la BigTech ?
<!-- .element: class="fragment" data-fragment-index="2" -->

----

## Fracture numérique

1. Accès
2. Formation
3. Impact et pouvoir

----

## Décentralisation

Comment est conçu le réseau Internet ?

* Nature du réseau Internet<!-- .element: class="fragment" data-fragment-index="1" -->
* Problèmes de la centralisation<!-- .element: class="fragment" data-fragment-index="1" -->
* Tout le monde peut créer un bout d'Internet<!-- .element: class="fragment" data-fragment-index="1" -->

----

## Licences libres

* Confiance
* Intéropérabilité
* Partage de la connaissance
* Pas que pour les logiciels
* Pas suffisant

----

## ...et...

Qu’est-ce que le cloud ?

* Libération des services<!-- .element: class="fragment" data-fragment-index="1" -->
* « cloud » = PC de quelqu'un d'autre<!-- .element: class="fragment" data-fragment-index="1" -->
* le LL = un moyen pas une fin<!-- .element: class="fragment" data-fragment-index="1" -->
* Communauté<!-- .element: class="fragment" data-fragment-index="1" -->
* Communs<!-- .element: class="fragment" data-fragment-index="1" -->
* Définition de liberté<!-- .element: class="fragment" data-fragment-index="1" -->

---

# Écologie

----

## Mise en garde

* les préocupations environnementales du numérique sont récentes
* pas de réel consensus méthodologique, ni d'objectivité des résultats & multiplicités de parametres possibles
* *exemples :* emissions de GES des équipements, poids des pages webs, consommations éléctriques

----

## Mise en garde

* Au-delà du *GreenIT*, posture de prudence : sujet d'une grande complexité.
* pour une part montante de travaux publics, le secteur numérique "n'offriraient aucunes garanties sur la question environnementale".
* Optimisations *vs* effet d'empilements et ambitions productivistes

----

## Matérialité numérique

* **4% des émissions mondiales de $CO_2$**
    * 47% : équipements utilisateurs
    * 53% : *Data Centers* et infrastructures réseaux
*Chiffres de l'ADEME*

----

## Matérialité numérique

![composition smartphone](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.x_snkkgesBNxSCNsWOOYUQHaIX%26pid%3DApi&f=1 =200x)

----

## Matérialité numérique

* Internet : infrastructure d'interconnexion de réseaux.
* Web : service permettant l'accès à des informations sur Internet ;
* Cloud : des serveurs informatiques distants
* des équipements-relais, des cables *back-bones*, des entrepots d'ordinateurs...

----

### Ressources Numériques

* une cinquantaine de métaux ;
* de l'énergie pour l'extraction, le transport, la transformation (charbon, pétrol...);
* des matières premières (caoutchou, eau...) ;
* fabrication de cables, d'ordinateurs, de smartphones, de box...

----

### Centres de données

* Des rangées de serveurs manipulant des données
* Produit de la chaleur et des calculs
* Occupe des surfaces importantes et influence le marché foncier

----

### Centres de données

![Google Data Center, Oregon](https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Google_Data_Center%2C_The_Dalles.jpg/1920px-Google_Data_Center%2C_The_Dalles.jpg =350x)

----

### Cables sous-marins

![image alt](https://veillecarto2-0.fr/wp-content/uploads/2019/01/maillage_mondial_c%C3%A2ble_telegeography.png =350x)

----

### Equipements utilisateurs

* Tout équipement connecté transmet des données et consomme de l'énergie
* Volume (2019) : 37 milliards d'équipements informatiques

----

### Consommation électrique

* Fonctionnement d'Internet : 7% de la consommation électrique mondiale
* de nombreux progrès en matière d'efficacité énergétique
* or, pas de diminution de la consommation électrique
* effet rebond !
* :warning: efficacité énergétique et consommation electrique ne sont pas des indicateurs fiables de la consommation énergétique réelle

----

### Un numérique matériel

* système matériel
* extraction, transport et transformation globales
* consommation d'énergies
* produit des GES + des déchets tout au long de son cycle de vie

----

## Cycle de Vie du matériel
*Matières premières:* usage de polluants, terres rares, usage de l'eau

*Fabrication:* beaucoup de composants

*Valorisation et fin de vie:* très peu de recyclage (seulement 15% des téléphones sont recyclés), pollution, déchets

Durée de vie très courte encouragée par les campagnes marketing (surconsommation)

----

## Sobriété numérique

Démarche visant à réduire l'impact environnemental du numérique

> Le numérique ne peut pas être écologique par nature : il faut chercher à le rendre plus sobre.

----

## Eco-gestes numériques
* **Réduire sa consommation**
    * Se désabonner des newsletters inutiles (Cleanfox)
    * Désinstaller les applications addictives / fantômes
* **Mieux consommer**
    * Utiliser le "mode sombre"
    * Privilégier le téléchargement (*légal*) à la lecture en ligne
    * Eteindre ses appareils
    * Privilégier le WIFI aux données cellulaires
    * Garder ses appareils le plus longtemps possible

---

## Les positions des institutions publiques en France

(11/11/2020)

---

# Politique

1. Gouvernance
2. Modèles économique
3. Communs

----

## Gouvernance

Parmi ces projets, lequel n’a pas connu un fork majeur dû à une divergence de point de vue entre la communauté et le mainteneur ?

* Plein de modèles
* Verticalité *vs.* Horizontalité
* Orientation du projet
* *Fork*
<!-- .element: class="fragment" data-fragment-index="1" -->

----

## Éthique

Sur une échelle de 1 à 5, à quel point trouvez-vous que le mot *politisé* est négatif ?

* *Funkwhale* *vs.* *Gab*
* Un missile lancé grâce à des logiciels libres est plus ethique ?
* Rendre service à tout le monde
* Repolitisation
<!-- .element: class="fragment" data-fragment-index="1" -->

----

## Communs

* Tragédie des communs
* Solutions de Hardin
* Réponse de Elinor Ostrom

----

## Modèles économique

* *Open Source* / Logiciel libre
* *SaaS*
* Support
* Fonctionnalités payantes
* générosité de la communauté
* Mécènat

---

# Conclusion
