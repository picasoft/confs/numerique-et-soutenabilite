# Numérique et Soutenabilité

Conférence donnée pour la SEDD P21.

Le texte est dans [conducteur.md](./conducteur.md) et les diapos dans [slides.md](slides.md).

Ces deux documents sont à injecter dans CodiMD pour un bon rendu.

Contacts : Pierre LR, Rémi, Romain DL, Roxane et Lucas qui ont réalisés et présenté la conf, pour des conseils sur la réutilisation.
